public class Exercise03 {
    public static void main(String[] args) {
        int count = 11;
        outputFibonacciNumbersWhileLoop(count);
        System.out.println();
        outputFibonacciNumbersForLoop(count);
    }

    private static void outputFibonacciNumbersWhileLoop(int count) {
        int previousNumber = 1;
        int currentNumber = 1;
        System.out.print("While loop:\t" + previousNumber + " " + currentNumber + " ");
        while(count > 2) {
            int temporary = currentNumber;
            currentNumber += previousNumber;
            previousNumber = temporary;
            System.out.print(currentNumber + " ");
            count--;
        }
    }

    private static void outputFibonacciNumbersForLoop(int count) {
        int previousNumber = 1;
        int currentNumber = 1;
        System.out.print("For loop:\t" + previousNumber + " " + currentNumber + " ");
        for (int i = 2; i < count; i++) {
            int temporary = currentNumber;
            currentNumber += previousNumber;
            previousNumber = temporary;
            System.out.print(currentNumber + " ");
        }
    }
}